$(document).ready(function() {
		
	$('#hamburger').click(function(){
		$(this).toggleClass('open');
		$('.listing_links , body').toggleClass('active');
	});


	// Hide header on scroll down
	var didScroll;
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = 0;

    $(window).scroll(function(event) {
        didScroll = true;

        var height = $(window).scrollTop();

        if (height > lastScrollTop && height > navbarHeight) {
            $('#header_main').removeClass('nav-down').addClass('nav-up');
        } else {
            if (height + $(window).height() < $(document).height()) {
                $('#header_main').removeClass('nav-up').addClass('nav-down');
            }
        }

        lastScrollTop = height;
    });


	$(window).scroll(function() {
	if ($(this).scrollTop() == 0){  
	    $('#header_main').addClass("transparent");
	  } else {
	  	$('#header_main').removeClass("transparent");
	  }
	});


	$('.nice').niceSelect();

});